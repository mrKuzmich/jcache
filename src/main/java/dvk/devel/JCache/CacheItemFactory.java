package dvk.devel.JCache;

public interface CacheItemFactory<V> {
    public CacheItem<V> createItem(V value) throws Exception;
}
