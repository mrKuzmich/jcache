package dvk.devel.JCache;

import java.io.*;

/**
 * Элемент кэша с сохранением в файл. <br/>
 * Сохранение каждого элемента происходит в отдельном файле (хотя практичнее было бы писать все в один и осушествлять
 * в нем поиск по некоему индексу, но это уже другая задача)
 * @param <V>
 */
public class FileCacheItem<V extends Serializable> implements CacheItem<V> {
    private final File fValue;

    public FileCacheItem(V value, File fValue) throws IOException {
        this.fValue = fValue;
        writeValue(value);
    }

    @Override
    protected void finalize() throws Throwable {
        freeResourse();
        super.finalize();
    }

    // освобождает занятые ресурсы
    public void freeResourse() {
        if (fValue.exists() && !fValue.delete())
            fValue.deleteOnExit();
    }

    @Override
    public V getValue() throws IOException {
        try (final ObjectInputStream oos = new ObjectInputStream(new FileInputStream(fValue))) {
            return (V) oos.readObject(); // todo добавить проверку
        } catch (ClassNotFoundException e) {
            return null; // todo ???
        }
    }

    public void writeValue(V value) throws IOException {
        try (final ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fValue))) {
            oos.writeObject(value);
            oos.flush();
        }
    }

}
