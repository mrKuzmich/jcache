package dvk.devel.JCache;

public class MemCacheItem<V> implements CacheItem<V> {
    private final V value;

    public MemCacheItem(V value) {
        this.value = value;
    }

    @Override
    public V getValue() {
        return this.value;
    }
}
