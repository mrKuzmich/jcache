package dvk.devel.JCache;

public interface Cache<K, V> {
    /**
     * Добавляет элемент value с ключем key в кэш
     *
     * @param key   ключ добавляемого элемента
     * @param value добавляемый элемент
     * @return true если элемент был успешно добавлен
     */
    public boolean add(K key, V value);

    /**
     * Возвращает элемент с ключом key из кэша. Если элемент не найден, возвращается null.
     *
     * @param key ключ элемнта кэша
     * @return элемент кэша соответствующий заданному ключу или null в случае, если такой элемент не найден
     */
    public V get(K key);

    /**
     * Удаляет элемент кэша с ключом key
     *
     * @param key ключ элемента кэша для удаления
     * @return true, если элемент был найден и удален
     */
    public boolean remove(K key);
}
