package dvk.devel.JCache;

public class MemCacheItemFactory<V> implements CacheItemFactory<V> {
    @Override
    public CacheItem<V> createItem(V value) {
        return new MemCacheItem<V>(value);
    }
}
