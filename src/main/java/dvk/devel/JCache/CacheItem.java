package dvk.devel.JCache;

public interface CacheItem<V> {
    public V getValue() throws Exception;
}
