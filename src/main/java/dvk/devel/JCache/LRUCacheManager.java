package dvk.devel.JCache;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

public class LRUCacheManager<K, V> implements CacheManager<K, V> {
    private Map<Long, K> map = new ConcurrentSkipListMap<Long, K>();


    @Override
    public K getRemovedKey() {
        return null;
    }

    @Override
    public void add(Cache<K, V> cache, K key) {
        map.put(System.currentTimeMillis(), key);
    }

    @Override
    public void get(Cache<K, V> cache, K key) {
        // в данной реадлизации стратегии обращение к элементу кеша не учитывается
    }
}
