package dvk.devel.JCache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class CacheImpl<K, V> implements Cache<K, V> {
    private volatile Map<K, CacheItem<V>> cache = null;
    private int cacheSize;
    private CacheManager<K, V> manager;

    public CacheImpl(int cacheSize, CacheManager<K, V> manager) {
        this.cacheSize = cacheSize;
        this.manager = manager;
    }

    // todo: а нужен он как public???
    public Map<K, CacheItem<V>> getCache() {
        if (cache == null)
            synchronized (this) {
                if (cache == null)
                    cache = createCache();
            }
        return cache;
    }

    /**
     * Создает Map-хранилище для кэша. Можно перекрыть для определения своего класса хранилища.
     * @return Map&lt;K, CacheItem&lt;V&gt;&gt;
     */
    public Map<K, CacheItem<V>> createCache() {
        // для универсальности ConcurrentHashMap, если многопоточность не нужна можно заменить на более быстрый
        return new ConcurrentHashMap<>(cacheSize);
    }

    /**
     * Создает элемент кэша CacheItem&lt;V&gt;<br/>
     * TODO: Может заменить на фабрику?
     * @param value значение
     * @return CacheItem&lt;V&gt;
     */
    abstract CacheItem<V> createCacheItem(V value);

    @Override
    public boolean add(K key, V value) {
        // мог поменяться размер, поэтому чистим в цикле
        while (getCache().size() >= cacheSize)
            freeCache();
        getCache().put(key, createCacheItem(value));
        manager.add(this, key); // оповешаем менаджер
        return true;
    }

    @Override
    public V get(K key) {
        manager.get(this, key);
        return null;
    }

    @Override
    public boolean remove(K key) {
        return getCache().remove(key) != null;
    }

    private boolean freeCache() {
        return manager != null &&
                remove(manager.getRemovedKey());
    }
}
