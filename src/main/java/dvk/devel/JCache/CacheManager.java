package dvk.devel.JCache;

public interface CacheManager<K, V> {
    public K getRemovedKey();
    public void add(Cache<K, V> cache, K key);
    public void get(Cache<K, V> cache, K key);
}
