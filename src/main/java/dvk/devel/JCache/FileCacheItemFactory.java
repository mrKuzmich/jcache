package dvk.devel.JCache;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class FileCacheItemFactory<V extends Serializable> implements CacheItemFactory<V> {
    private final File cacheDirectory;

    public FileCacheItemFactory(File cacheDirectory) {
        this.cacheDirectory = cacheDirectory;
    }

    @Override
    public CacheItem<V> createItem(V value) throws IOException {
        return new FileCacheItem<V>(value, getItemFile(value));
    }

    public File getItemFile(V value) {

    }
}
